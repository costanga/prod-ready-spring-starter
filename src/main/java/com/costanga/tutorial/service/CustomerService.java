package com.costanga.tutorial.service;

import com.costanga.tutorial.dto.CustomerDto;

import java.util.List;

public interface CustomerService {
    List<CustomerDto> findAllCustomers();
    List<CustomerDto> findCustomersByFirstName(String firstName);
}
