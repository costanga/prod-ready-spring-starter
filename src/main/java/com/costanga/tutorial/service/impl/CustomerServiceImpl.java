package com.costanga.tutorial.service.impl;

import com.costanga.tutorial.dto.CustomerDto;
import com.costanga.tutorial.model.Customer;
import com.costanga.tutorial.repository.CustomerRepository;
import com.costanga.tutorial.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerServiceImpl implements CustomerService {
    private CustomerRepository customerRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<CustomerDto> findAllCustomers() {
        List<Customer> customers = customerRepository.findAll();
        return customers.stream().map((customer) -> mapToCustomerDto(customer)).collect(Collectors.toList());
    }

    @Override
    public List<CustomerDto> findCustomersByFirstName(String firstName) {
        List<Customer> customers = customerRepository.findByFirstName(firstName);
        return customers.stream().map((customer) -> mapToCustomerDto(customer)).collect(Collectors.toList());
    }

    private CustomerDto mapToCustomerDto(Customer customer) {
        CustomerDto customerDto = CustomerDto.Builder
                .newInstance()
                .setId(customer.getId())
                .setFirstName(customer.getFirstName())
                .setLastName(customer.getLastName())
                .build();
        return customerDto;
    }
}
