package com.costanga.tutorial.controller;

import com.costanga.tutorial.dto.CustomerDto;
import com.costanga.tutorial.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CustomerController {
    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping(path="/customer/getAll", produces = "application/json")
    public List<CustomerDto> getCustomers() {
        List<CustomerDto> customers = customerService.findAllCustomers();
        return customers;
    }

    @GetMapping("/customer")
    public List<CustomerDto> getCustomerByFirstName(@RequestParam(value = "firstName") String firstName) {
        List<CustomerDto> customers = customerService.findCustomersByFirstName(firstName);
        return customers;
    }
}
