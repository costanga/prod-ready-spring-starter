package com.costanga.tutorial.dto;

public class CustomerDto {
    private final Long id;
    private final String firstName;
    private final String lastName;

    private  CustomerDto(Long id, String firstName, String lastName) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    private CustomerDto(Builder builder) {
        this.id = builder.id;
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
    }

    public Long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public static class Builder{
        private Long id;
        private String firstName;
        private String lastName;

        public static Builder newInstance()
        {
            return new Builder();
        }

        private Builder() {}

        public Builder setId(Long id) {
            this.id = id;
            return this;
        }

        public Builder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public CustomerDto build() {
            return new CustomerDto(this);
        }
    }
}

