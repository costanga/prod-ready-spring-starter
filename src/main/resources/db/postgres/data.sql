INSERT INTO customers (first_name, last_name) SELECT 'James', 'Carter' WHERE NOT EXISTS (SELECT * FROM customers WHERE id=1);
INSERT INTO customers (first_name, last_name) SELECT 'Helen', 'Leary' WHERE NOT EXISTS (SELECT * FROM customers WHERE id=2);
INSERT INTO customers (first_name, last_name) SELECT 'Linda', 'Douglas' WHERE NOT EXISTS (SELECT * FROM customers WHERE id=3);
INSERT INTO customers (first_name, last_name) SELECT 'Rafael', 'Ortega' WHERE NOT EXISTS (SELECT * FROM customers WHERE id=4);
INSERT INTO customers (first_name, last_name) SELECT 'Henry', 'Stevens' WHERE NOT EXISTS (SELECT * FROM customers WHERE id=5);
INSERT INTO customers (first_name, last_name) SELECT 'Sharon', 'Jenkins' WHERE NOT EXISTS (SELECT * FROM customers WHERE id=6);