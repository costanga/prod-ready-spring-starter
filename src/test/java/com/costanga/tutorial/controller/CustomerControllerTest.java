package com.costanga.tutorial.controller;


import com.costanga.tutorial.dto.CustomerDto;
import com.costanga.tutorial.service.CustomerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CustomerControllerTest {
    @InjectMocks
    CustomerController customerController;

    @Mock
    CustomerService customerService;

    @Test
    public void testGetCustomers() {
        CustomerDto customerDto1 = CustomerDto.Builder
                .newInstance()
                .setId(1L)
                .setFirstName("John")
                .setLastName("Smith")
                .build();

        CustomerDto customerDto2 = CustomerDto.Builder
                .newInstance()
                .setId(2L)
                .setFirstName("Jane")
                .setLastName("Doe")
                .build();

        List<CustomerDto> customers = new ArrayList<CustomerDto>();
        customers.add(customerDto1);
        customers.add(customerDto2);

        when(customerService.findAllCustomers()).thenReturn(customers);

        List<CustomerDto> result = customerController.getCustomers();

        assertThat(result.size()).isEqualTo(2);
        assertThat(result.get(0).getFirstName()).isEqualTo(customerDto1.getFirstName());
        assertThat(result.get(1).getFirstName()).isEqualTo(customerDto2.getFirstName());
    }

    @Test
    public void testGetCustomerByFirstName() {
        CustomerDto customerDto1 = CustomerDto.Builder
                .newInstance()
                .setId(1L)
                .setFirstName("John")
                .setLastName("Doe")
                .build();

        List<CustomerDto> customers = new ArrayList<CustomerDto>();
        customers.add(customerDto1);

        when(customerService.findCustomersByFirstName("John")).thenReturn(customers);

        List<CustomerDto> result = customerController.getCustomerByFirstName("John");

        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0).getFirstName()).isEqualTo(customerDto1.getFirstName());
    }
}
