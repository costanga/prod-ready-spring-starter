package com.costanga.tutorial.service;

import com.costanga.tutorial.dto.CustomerDto;
import com.costanga.tutorial.model.Customer;
import com.costanga.tutorial.repository.CustomerRepository;
import com.costanga.tutorial.service.impl.CustomerServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceImplTest {
    @InjectMocks
    CustomerServiceImpl customerServiceImpl;

    @Mock
    CustomerRepository customerRepository;

    @Test
    public void  testFindAllCustomers() {
        Customer customer1 = new Customer(1L, "Jane", "Doe");
        Customer customer2 = new Customer(2L, "John", "Doe");

        List<Customer> customers = new ArrayList<Customer>();
        customers.add(customer1);
        customers.add(customer2);

        CustomerDto customerDto1 = CustomerDto.Builder
                .newInstance()
                .setId(1L)
                .setFirstName("John")
                .setLastName("Smith")
                .build();

        CustomerDto customerDto2 = CustomerDto.Builder
                .newInstance()
                .setId(2L)
                .setFirstName("Jane")
                .setLastName("Doe")
                .build();

        when(customerRepository.findAll()).thenReturn(customers);
        List<CustomerDto> result = customerServiceImpl.findAllCustomers();

        assertThat(result.size()).isEqualTo(2);
        assertThat(result.get(0).getId()).isEqualTo(customerDto1.getId());
        assertThat(result.get(1).getId()).isEqualTo(customerDto2.getId());
    }

    @Test
    public void  testFindCustomersByFirstName() {
        Customer customer1 = new Customer(1L, "Jane", "Doe");

        List<Customer> customers = new ArrayList<>();
        customers.add(customer1);

        CustomerDto customerDto = CustomerDto.Builder
                .newInstance()
                .setId(2L)
                .setFirstName("Jane")
                .setLastName("Doe")
                .build();

        List<CustomerDto> customersDtoList = new ArrayList<>();
        customersDtoList.add(customerDto);

        when(customerRepository.findByFirstName("Jane")).thenReturn(customers);
        List<CustomerDto> result = customerServiceImpl.findCustomersByFirstName("Jane");

        assertThat(result.size()).isEqualTo(1);
        assertThat(result.get(0).getFirstName()).isEqualTo(customerDto.getFirstName());
    }
}

